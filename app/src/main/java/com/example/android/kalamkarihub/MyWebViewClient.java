package com.example.android.kalamkarihub;

import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Created by Kamal Dev Sharma on 6/26/2017.
 */

class MyWebViewClient extends WebViewClient {
    private ProgressBar progressBar;
    public MyWebViewClient(ProgressBar progressBar) {
        this.progressBar=progressBar;
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
    }
    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);
        progressBar.setVisibility(View.GONE);
    }
}
